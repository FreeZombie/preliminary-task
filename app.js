const util = require('util');

const persons = Object.freeze([ //Don't edit this array
 {
 userId: 1,
 name: "Teppo Testaaja",
 dateOfBirth: new Date("1959-01-01"),
 email: "teppo.testaaja@buutti.com"
 },
 {
 userId: 2,
 name: "Tessa Testaaja",
 dateOfBirth: new Date("1981-01-01"),
 email: "tessa.testaaja@buutti.com"
 },
 {
 userId: 3,
 name: "Teuvo Testaaja",
 dateOfBirth: new Date("1989-05-05"),
 email: "teuvo.testaaja@buutti.com"
 },
 {
 userId: 4,
 name: "Outi Ohjelmoija",
 dateOfBirth: new Date("1972-06-06"),
 email: "outi.ohjelmoija@buutti.com"
 },
 {
 userId: 5,
 name: "Olli Ohjelmoija",
 dateOfBirth: new Date("1989-05-05"),
 email: "olli.ohjelmoija@buutti.com"
 },
 {
 userId: 6,
 name: "Teppo Ohjelmoija",
 dateOfBirth: new Date("1980-02-02"),
 email: "teppo.ohjelmoija@buutti.com"
 }
]);
const professions = Object.freeze([
 {
 userId: 1,
 workplace: "Some Company",
 position: "Manager"
 },
 {
 userId: 2,
 workplace: "Epic Company",
 position: "System admin"
 },
 {
 userId: 3,
 workplace: "Some Company",
 position: "Developer"
 },
 {
 userId: 4,
 workplace: "Some Company",
 position: "Manager"
 },
 {
 userId: 5,
 workplace: "Epic Company",
 position: "System admin"
 },
 {
 userId: 6,
 workplace: "Epic company",
 position: "Developer"
 }
]);
const interests = Object.freeze([
 {
 userId: 1,
 interest: "Cats"
 },
 {
 userId: 1,
 interest: "Computers"
 },
 {
 userId: 2,
 interest: "Ice hockey"
 },
 {
 userId: 3,
 interest: "Computers"
 },
 {
 userId: 3,
 interest: "Cats"
 },
 {
 userId: 3,
 interest: "Football"
 },
 {
 userId: 4,
 interest: "Computers"
 },
 {
 userId: 4,
 interest: "Epicness"
 },
 {
 userId: 5,
 interest: "Computers"
 },
 {
 userId: 6,
 interest: "Fishing"
 },
 {
 userId: 6,
 interest: "Cats"
 }
])
/*
 It's enough to make this function work with the
 'persons' array above. You don't have to consider or defend
 against any other type of names. */
const findByFirstName = (firstname) => {
    var results = [];
    persons.forEach(function(obj){
        if (obj.name.includes(firstname)) {
            results.push(obj.name);
        }
    });
    return results.toString();
 };
/*
 Should return the users age.
 Age should be an integer.
 findPersonAge("Teuvo Testaaja") returns 31
*/
const findPersonAge = (name) => { //Age in years
 var result = persons.find(obj => obj.name === name);
 var today = new Date();
 var years = today.getFullYear() - result.dateOfBirth.getFullYear();
 var months = today.getMonth() - result.dateOfBirth.getMonth();
 if (months < 0 || months === 0 && today.getDate() < result.dateOfBirth.getDate()) {
    years = years-1; //years--; 
 }
 return years;
};
/*
Calculate the average age of all users in the persons array (full
years)
*/
const calculateAverageAge = () => {
 var ages = [];
 var total = 0;
 persons.forEach(function(person){
   age = findPersonAge(person.name)
   ages.push(age);
   total = total + age;
 }); 
 var average = total / ages.length;
 return (Math.round(average*100) / 100).toFixed(2);
};
/*
Turn the arrays of objects (persons, professions, interests) into a
new object of workplace profiles with employees of the
particular company listed as shown below.
Also include the interests of that particular
employee as a new array in the employeeObject.
Below is an example of how Some Company
profile object should look like:
{
 'Some Company': {
 employees: [
 {
 userId: 1,
 name: "Teppo Testaaja",
 dateOfBirth: 1959-01-01T00:00:00.000Z,
 email: "teppo.testaaja@buutti.com",
 position: "Manager",
 interests: ["Computers", "Cats"]
 },
 {
 userId: 3,
 name: "Teuvo Testaaja",
 dateOfBirth: 1989-05-05T00:00:00.000Z,
 email: "teuvo.testaaja@buutti.com",
 position: "Developer",
 interests: ["Computers", "Cats", "Football"]
 },
 {
 userId: 4,
 name: "Outi Ohjelmoija",
 dateOfBirth: 1972-06-06T00:00:00.000Z,
 email: "outi.ohjelmoija@buutti.com",
 position: "Manager",
 interests: ["Computers", "Epicness"]
 }
 ]
 },
 ...
}
*/
/* name: "Outi Ohjelmoija",
 dateOfBirth: new Date("1972-06-06"),
 email: "outi.ohjelmoija@buutti.com"*/
const createCompanyProfiles = () => {
 var companyProfile = {};
 var companyProfiles = [];
 var companies = []; 
 professions.forEach(function(obj){
  companies.push(obj.workplace);
 });
 companies = Array.from(new Set(companies));
 companies.forEach(function(company){
  var userIds = [];
  professions.forEach(function(emp){   
   if (emp.workplace === company) {
     userIds.push(emp.userId);
     //console.log("adding an employee by id " + emp.userId + " to company " + emp.workplace);
     //console.log(userIds);
   }   
   companyProfile[company] = [];
   companyProfile[company].employees = [];    
  });
  userIds.forEach(function (id){
    /*userId: 1,
     name: "Teppo Testaaja",
 dateOfBirth: 1959-01-01T00:00:00.000Z,
 email: "teppo.testaaja@buutti.com",
 position: "Manager",
 interests: ["Computers", "Cats"]*/
    //console.log("processing id " + obj);
    var employee = {};
    var personFromId = persons.find(obj => obj.userId === id);
    var professionFromId = professions.find(obj => obj.userId === id); //Tämän varmaankin olisi voinut tehdä tuolla aiemmassa kohtaa, mutta teempä nyt tässä.
    employee.userId = id;
    employee.name = personFromId.name;
    employee.dateOfBirth = personFromId.dateOfBirth;
    employee.email = personFromId.email;
    employee.position = professionFromId.position;
    companyProfile[company].employees.push(employee);
    var interestObjs = interests.filter(interest => interest.userId === id);  
    employee.interests = [];
    interestObjs.forEach(function(interestObj){
     employee.interests.push(interestObj.interest);
    });
   });  
  companyProfiles.push (companyProfile);
  //console.log(companyProfile);
  companyProfile = {};
 });
 //console.log(companyProfiles);
 //console.log(JSON.stringify(companyProfiles, null, ' '));
 //console.dir(companyProfiles);
 //console.table(companyProfiles);
 //console.log(Object.entries(companyProfiles));
 console.log(util.inspect(companyProfiles, { showHidden: false, depth: null }));
 return ' ';
 /*persons.forEach(function(person){
  var employee = [];
  employee.userId = person.userId;
  employee.name = person.name;
  employee.dateOfBirth = person.dateOfBirth;
  employee.email = person.email;
  employee.position = professions.find(obj => obj.userId === person.userId).position;
  var interestsTmp = interests.filter(interest => interest.userId === person.userId);  
  employee.interests = [];
  interestsTmp.forEach(function (obj){
   employee.interests.push(obj.interest); 
  });
  companyProfiles.push(employee);
 });*/
 //return companyProfiles;
}
console.log(
 "All persons with first name \"Teppo\" are",
 findByFirstName("Teppo")
);
console.log(
 "Calculate Person <Teuvo Testaaja> age",
 findPersonAge("Teuvo Testaaja")
);
console.log(
 "The average age of all persons",
 calculateAverageAge()
);
console.log(
 "Company profiles created",
 createCompanyProfiles()
);